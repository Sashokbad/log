#pragma once
#include <mutex>
#include <memory>
#include "log/logger/base_logger.hpp"

namespace log {

class Logger {
public:
  static Logger& get();
  BaseLogger& get_global_logger();
  void set_global_logger(std::unique_ptr<BaseLogger> logger);
private:
  Logger();

  std::mutex mutex_;
  std::unique_ptr<BaseLogger> logger_;
};

void d(const std::string& str);
void i(const std::string& str);
void w(const std::string& str);
void e(const std::string& str);

} //namespace log
