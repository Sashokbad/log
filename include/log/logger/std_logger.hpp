#pragma once
#include "log/logger/base_logger.hpp"

namespace log {

class StdoutLogger: public BaseLogger {
public:
  StdoutLogger();
};

class StderrLogger: public BaseLogger {
public:
  StderrLogger();
};
} //namespace log
