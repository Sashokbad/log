#pragma once
#include <iostream>

namespace log {

enum class Level {
  DEBUG = 0,
  INFO,
  WARNING,
  ERROR,
};

class BaseLogger {
public:
  BaseLogger();
  explicit BaseLogger(std::ostream* stream);
  virtual ~BaseLogger() = default;
  void log(Level level, const std::string& str);
  void d(const std::string& str);
  void i(const std::string& str);
  void w(const std::string& str);
  void e(const std::string& str);
  void set_level(Level level);
  Level level() const;

protected:
  std::ostream* stream_;
private:
  Level level_;
};

} //namespace log
