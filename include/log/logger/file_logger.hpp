#pragma once
#include "log/logger/base_logger.hpp"
#include <fstream>

namespace log {

class FileLogger: public BaseLogger {
public:
  explicit FileLogger(const std::string& path);
private:
  std::ofstream file_; 
};

} //namespace log
