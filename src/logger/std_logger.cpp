#include "log/logger/std_logger.hpp"

namespace log {

StdoutLogger::StdoutLogger():
  BaseLogger(&std::cout) {
}

StderrLogger::StderrLogger():
  BaseLogger(&std::cerr) {
}

} // namespace log
