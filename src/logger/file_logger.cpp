#include "log/logger/file_logger.hpp"

namespace log {

FileLogger::FileLogger(const std::string& path):
  file_{path} {
  stream_ = &file_;
}

} // namespace log
