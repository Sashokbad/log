#include "log/logger/base_logger.hpp"

namespace log {

BaseLogger::BaseLogger()
  : level_{},
    stream_{} {

}

BaseLogger::BaseLogger(std::ostream* stream)
  : level_{},
    stream_{stream} {
}

void BaseLogger::set_level(Level level) {
  level_ = level;
}

Level BaseLogger::level() const {
  return level_;
}

void BaseLogger::log(Level level, const std::string& str) {
  if (level > level_ && stream_ != nullptr) {
    *stream_ << str << std::endl;
  }
}

void BaseLogger::d(const std::string& str) {
  log(Level::DEBUG, str);
}

void BaseLogger::i(const std::string& str) {
  log(Level::INFO, str);
}

void BaseLogger::w(const std::string& str) {
  log(Level::WARNING, str);
}

void BaseLogger::e(const std::string& str) {
  log(Level::ERROR, str);
}

} // namespace log
