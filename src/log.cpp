#include "log/log.hpp"
#include <memory>
#include "log/logger/std_logger.hpp"

namespace log {

Logger::Logger()
  : logger_{std::make_unique<StdoutLogger>()} {
}

BaseLogger& Logger::get_global_logger() {
  std::lock_guard<std::mutex> lock_(mutex_);
  return *logger_;
}

void Logger::set_global_logger(std::unique_ptr<BaseLogger> logger) {
  std::lock_guard<std::mutex> lock_(mutex_);
  logger_ = std::move(logger);
}

Logger& Logger::get() {
  static Logger instance;
  return instance;
}

void d(const std::string& str) {
  Logger::get().get_global_logger().d(str);
}

void i(const std::string& str) {
  Logger::get().get_global_logger().i(str);
}

void w(const std::string& str) {
  Logger::get().get_global_logger().w(str);
}

void e(const std::string& str) {
  Logger::get().get_global_logger().e(str);
}

} // namespace log
