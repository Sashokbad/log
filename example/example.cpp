#include <iostream>
#include "log/log.hpp"
#include "log/logger/file_logger.hpp"
#include "log/logger/std_logger.hpp"


int main(int argc, char** argv) {
  log::FileLogger logger("123");
  logger.log(log::Level::WARNING, "123");
  log::StdoutLogger log2;
  log::StderrLogger log3;
  log2.w("123");
  log3.e("Error");

  log::Logger::get().set_global_logger(std::make_unique<log::FileLogger>("123"));

  return 0;
}
